package com.founder.fiance;

import com.founder.core.domain.FianceAl;
import com.founder.core.domain.FianceWx;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class FianceFNYY extends FianceBASE {

    @Override
    public String buildContent(List<?> list, String subsys) {

        if (CollectionUtils.isEmpty(list)) {
            XxlJobLogger.log("无数据：" + subsys);
            return null;
        }

        Object object = list.get(0);
        if (object instanceof FianceAl) {
            System.out.println("丰宁医院支付宝");
            return buildContentAli(super.toFianceAlList(list), subsys);
        } else if (object instanceof FianceWx) {
            System.out.println("丰宁医院微信");
            return buildContentWx(super.toFianceWxList(list), subsys);
        } else {
            return null;
        }
    }

    private String buildContentWx(List<FianceWx> list, String subsys) {
        return null;
    }

    private String buildContentAli(List<FianceAl> list, String subsys) {
        return null;
    }
}
