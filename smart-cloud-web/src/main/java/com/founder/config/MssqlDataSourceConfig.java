package com.founder.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryMssql",   //EntityManagerFactory引用
        transactionManagerRef = "transactionManagerMssql",      //transactionManager引用
        basePackages = {"com.founder.extend"})
public class MssqlDataSourceConfig {

    /**
     * 注入 mysql数据源
     */
    @Resource(name = "mssqlDataSource")
    private DataSource mssqlDataSource;

    /**
     * 注入JPA配置实体
     */
    @Autowired
    private JpaProperties jpaProperties;

    private Map<String, String> getVendorProperties() {
        jpaProperties.setDatabase(Database.SQL_SERVER);
        jpaProperties.setShowSql(true);
        Map<String,String> map = new HashMap<>();
        map.put("hibernate.dialect","org.hibernate.dialect.SQLServer2008Dialect");
        map.put("hibernate.hbm2ddl.auto","none");
        map.put("hibernate.naming.implicit-strategy","org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl");
        map.put("hibernate.naming.physical-strategy","org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
        jpaProperties.setProperties(map);
        return jpaProperties.getProperties();
    }

    @Bean(name = "entityManagerFactoryMssql")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryMssql(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(mssqlDataSource)
                .properties(getVendorProperties())
                .packages("com.founder.extend")
                .persistenceUnit("mssqlPersistenceUnit")
                .build();
    }

    @Bean(name = "entityManagerMssql")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactoryMssql(builder).getObject().createEntityManager();
    }

    @Bean(name = "transactionManagerMssql")
    public PlatformTransactionManager transactionManagerMssql(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryMssql(builder).getObject());
    }
}
